<?php

include_once('Labyrinthe.php');
include_once('LabyrintheSolver.php');
include_once('LabyrintheResult.php');

$labyrinthe = new Labyrinthe();
$labyrintheSolver = new LabyrintheSolver($labyrinthe);
$labyrintheSolver->resolve();
$labyrintheSolver->getResult();