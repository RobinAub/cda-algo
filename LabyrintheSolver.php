<?php

class LabyrintheSolver
{
    private object $labyrinthe;
    private object $result;
    private array $map;
    private int $moves;

    public function __construct(Labyrinthe $labyrinthe)
    {
        $this->labyrinthe = $labyrinthe;
        $this->moves = 0;
        $this->map = $this->labyrinthe->getMap();
    }

    public function resolve(): void
    {
        $start = $this->labyrinthe->getStart();
        $this->result = new LabyrintheResult();
        $this->check($start);
    }

    public function getResult(): void
    {
        foreach($this->result->getWay() as $way){
            echo "Chemin : " . implode(",", $way) . PHP_EOL;
        }
        if($this->result->getSolved() == true){
            echo sprintf("Labyrinthe terminé en %d étapes" . PHP_EOL, $this->result->getMoves());
        }

    }

    private function check($start): void
    {
        $this->map[$start[0]][$start[1]] = 2;

        $directions = [
            "right" => [$start[0], $start[1] + 1],
            "left" => [$start[0], $start[1] - 1],
            "down" => [$start[0] + 1, $start[1]],
            "up" => [$start[0] - 1, $start[1]],
        ];

        foreach($directions as $direction){
            $this->addResult($direction);
        }
    }

    private function addResult($direction): void
    {
        
        if(isset($this->map[$direction[0]][$direction[1]])){
            if($this->map[$direction[0]][$direction[1]] == 1){
                $this->result->addWay($direction);
                $this->moves += 1;
                if($this->checkEnd($direction)){
                    $this->result->setSolved(true);
                    $this->result->setMoves($this->moves);
                    return;
                }
                $this->check($direction);
                return;
            }
        }
    }

    private function checkEnd($current): bool
    {
        $end = $this->labyrinthe->getEnd();
        if($end == $current){
            return true;
        }
        return false;
    }
}