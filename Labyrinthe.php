<?php

class Labyrinthe
{
    private array $map;
    private array $start;
    private array $end;

    public function __construct()
    {
        $this->map = [
            [1, 1, 1, 1],
            [0, 0, 0, 1],
            [0, 1, 1, 1],
            [0, 1, 0, 0],
            [0, 1, 1, 1]
        ];
        $this->start = [0, 0];
        $this->end = [4, 2];
    }

    public function getMap(): array
    {
        return $this->map;
    }

    public function getStart(): array
    {
        return $this->start;
    }

    public function getEnd(): array
    {
        return $this->end;
    }
}