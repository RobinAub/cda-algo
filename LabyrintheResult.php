<?php

class LabyrintheResult
{
    private array $way;
    private bool $solved;
    private int $moves;

    public function __construct()
    {
        $this->way = [];
    }

    public function getWay(): array
    {
        return $this->way;
    }

    public function addWay($way): void
    {
        array_push($this->way, $way);
    }
    
    public function getSolved(): bool
    {
        return $this->solved;
    }

    public function setSolved($solved): void
    {
        $this->solved = $solved;
    }

    public function getMoves(): int
    {
        return $this->moves;
    }

    public function setMoves($moves): void
    {
        $this->moves = $moves;
    }
}